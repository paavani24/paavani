#include <stdio.h>
#include <math.h>

float main()
{
	float x1, y1, x2, y2, dist;
	printf("Enter the points\n");
	scanf("%f%f%f%f", &x1, &x2, &y1, &y2);
	dist = sqrt(((x2 - x1)*(x2-x1)) + ((y2 - y1)*(y2-y1)));
	printf("The distance between points=%f\n", dist);
	return 0;
}
  