#include <stdio.h>
#include <math.h>

float input(float *x1,float *x2,float *y1,float *y2)
{
	printf("Enter the points\n");
	scanf("%f%f%f%f",x1,x2,y1,y2);
}
float compute(float x1,float x2,float y1,float y2)
{
    float distance;
    distance=sqrt(((x2-x1)*(x2-x1))+((y2-y1)*(y2-y1)));
    return distance;
}
void output(float distance)
{
    printf("The distance=%f\n",distance);
}
int main()
{
    float a,b,c,d,dis;
    input(&a,&b,&c,&d);
    dis=compute(a,b,c,d);
    output(dis);
    return 0;
}
     